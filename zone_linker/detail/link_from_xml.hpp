///////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code to define links between zones.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Defines.
//##############################################################################

#define   S_LINK            "link"

//##############################################################################
// Includes.
//##############################################################################

#include "zone_linker/base/zone_linker_base.hpp"

#ifndef NNP_ZONE_LINKER_DETAIL_HPP
#define NNP_ZONE_LINKER_DETAIL_HPP

namespace wn_user
{

namespace detail
{

//##############################################################################
// Types.
//##############################################################################

typedef
boost::tuple< std::string, std::string, std::string, double > link_tuple_t;

typedef std::map<size_t, link_tuple_t> link_map_t;

//##############################################################################
// zone_linker().
//##############################################################################

class zone_linker : public zone_linker_base
{

  public:
    zone_linker() : zone_linker_base() {}
    zone_linker( v_map_t& v_map ) : zone_linker_base( v_map ) {}

    static void
    my_links_function(
      const char * s_name,
      const char * s_tag1,
      const char * s_tag2,
      const char * s_value,
      link_map_t& link_map
    )
    {
    
      if( strcmp( s_name, S_LINK ) )
      {
        std::cerr << "Property is not a link." << std::endl;
        exit( EXIT_FAILURE );
      }
    
      size_t i_link = boost::lexical_cast<size_t>( s_tag1 );
    
      if( link_map.find( i_link )  == link_map.end() )
      {
        link_map[i_link] = boost::make_tuple( "0", "0", "0", 0. );
      }
    
      if( strcmp( s_tag2, "label1" ) == 0 )
        boost::get<0>( link_map.find( i_link )->second ) = s_value;
      else if( strcmp( s_tag2, "label2" ) == 0 )
        boost::get<1>( link_map.find( i_link )->second ) = s_value;
      else if( strcmp( s_tag2, "label3" ) == 0 )
        boost::get<2>( link_map.find( i_link )->second ) = s_value;
      else if( strcmp( s_tag2, "weight" ) == 0 )
        boost::get<3>( link_map.find( i_link )->second ) =
          boost::lexical_cast<double>( s_value );
      else
      {
        std::cerr << "Link contains non-valid data." << std::endl;
      }
    
    }

    template<class Graph>
    void
    setLinks( Graph& g )
    {
    
      user::zone_link_graph_t::vertex_iterator vi, vi_end;
      user::zone_link_graph_t::edge_descriptor e;
      user::vertex_multi_index vm;
      bool e_add;
    
      user::fill_multi_zone_vertex_hash( g, vm );
    
      boost::tie( vi, vi_end ) = boost::vertices( g );
    
      for( ; vi != vi_end; vi++ )
      {
    
        link_map_t my_link_map;
    
        Libnucnet__Zone__iterateOptionalProperties(
          g[*vi].getNucnetZone(),
          "link",
          NULL,
          NULL,
          (Libnucnet__Zone__optional_property_iterate_function)
            my_links_function,
          &my_link_map
        );
    
        for(
          link_map_t::iterator it = my_link_map.begin();
          it != my_link_map.end();
          it++
        )
        {
    
          boost::tie( e, e_add ) =
             boost::add_edge(
               *vi,
               user::get_vertex_from_multi_zone_hash(
                 vm,
                 it->second.get<0>(),
                 it->second.get<1>(),
                 it->second.get<2>()
               ),
               g
             );
    
          g[e].setWeight( it->second.get<3>() );
    
        }
    
      }
    
    }
    
};

//##############################################################################
// zone_linker_options().
//##############################################################################

class zone_linker_options : public zone_linker_base_options
{

  public:
    zone_linker_options() : zone_linker_base_options() {}

    void
    getDetailOptions( po::options_description& zone_linker )
    {
      getBaseOptions( zone_linker );
    }

    std::string
    getDetailExample()
    {
      return getBaseExample();
    }

};

} // namespace detail

} // namespace wn_user

#endif  // NNP_ZONE_LINKER_DETAIL_HPP

