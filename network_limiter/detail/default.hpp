////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file network_limiter.hpp
//! \brief A file to define network limiter routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "user/network_limiter.h"

#ifndef NNP_NETWORK_LIMITER_DETAIL_HPP
#define NNP_NETWORK_LIMITER_DETAIL_HPP

#define S_LIMIT_ABUNDS   "limit_abunds"
#define S_SMALL_ABUNDS   "small_abunds"
#define S_SMALL_RATES    "small_rates"


/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// network_limiter().
//##############################################################################

class network_limiter
{

  public:
    network_limiter(){}
    network_limiter( v_map_t& v_map )
    {
      dLimitAbunds = v_map[S_LIMIT_ABUNDS].as<double>();
      dSmallAbunds = v_map[S_SMALL_ABUNDS].as<double>();
      dSmallRates = v_map[S_SMALL_RATES].as<double>();
    }

    void operator()( nnt::Zone& zone )
    {
      limit_network( zone );
    }

    void operator()( std::vector<nnt::Zone>& zones )
    {
#ifndef NO_OPENMP
      #pragma omp parallel for schedule( dynamic, 1 )
#endif
      for( size_t i = 0; i < zones.size(); i++ )
      {
        limit_network( zones[i] );
      }
    }

  private:
    double dLimitAbunds, dSmallAbunds, dSmallRates;

    void limit_network( nnt::Zone& zone )
    {
      user::zero_out_small_abundances( zone, dSmallAbunds );
      user::limit_evolution_network( zone, dLimitAbunds );
      zone.updateProperty( nnt::s_SMALL_RATES_THRESHOLD, dSmallRates );
    }

};
     
//##############################################################################
// network_limiter_options().
//##############################################################################

class network_limiter_options
{

  public:
    network_limiter_options(){}

    void
    get( po::options_description& network_limiter )
    {

      try
      {

        network_limiter.add_options()

        // Option for value of small abundances threshold
        (  S_LIMIT_ABUNDS,
           po::value<double>()->default_value( 1.e-25, "1.e-25" ),
           "Limiting abundances threshold" )

        // Option for value of small abundances threshold
        (  S_SMALL_ABUNDS,
           po::value<double>()->default_value( 1.e-50, "1.e-50" ),
           "Small abundances threshold (to zero out)" )

        // Option for value of small rates threshold
        (  S_SMALL_RATES,
           po::value<double>()->default_value( 1.e-25, "1.e-25" ),
           "Small rates threshold" )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
   }

};

} // namespace detail

} // namespace wn_user 

#endif // NNP_NETWORK_LIMITER_DETAIL_HPP
