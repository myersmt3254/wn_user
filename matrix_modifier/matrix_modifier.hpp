////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file matrix_modifier.hpp
//! \brief A file to define matrix modification routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"

#ifndef NNP_MATRIX_MODIFIER_HPP
#define NNP_MATRIX_MODIFIER_HPP

/**
 * @defgroup matrix_modifier matrix_modifier
 *
 * @{
 */


namespace wn_user
{

//##############################################################################
// matrix_modifier().
//##############################################################################

class matrix_modifier : public detail::matrix_modifier
{

  public:
    matrix_modifier( v_map_t& v_map ) : detail::matrix_modifier( v_map ){}

};

//##############################################################################
// matrix_modifier_options().
//##############################################################################

class matrix_modifier_options : public detail::matrix_modifier_options
{

  public:
    matrix_modifier_options() : detail::matrix_modifier_options(){}

    std::string
    getExample()
    {
      return detail::matrix_modifier_options::getExample();
    }

    void
    get( options_map& o_map )
    {

      try
      {

        po::options_description
          matrix_modifier("\nMatrix modifications options");

        detail::matrix_modifier_options::getOptions( matrix_modifier );

        o_map.insert(
          std::make_pair<std::string, options_struct>(
            "matrix_modifier",
            options_struct( matrix_modifier, getExample() )
          )
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

}  // namespace wn_user

#endif // NNP_MATRIX_MODIFIER_HPP

/** @} */

