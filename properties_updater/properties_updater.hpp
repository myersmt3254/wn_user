////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
///
/// \file
///
////////////////////////////////////////////////////////////////////////////////

#include <vector>
#include <boost/ptr_container/ptr_vector.hpp>

#include "my_global_types.h"

#ifndef NNP_PROPERTIES_UPDATER_HPP
#define NNP_PROPERTIES_UPDATER_HPP

namespace wn_user
{

namespace properties_updater
{

//##############################################################################
// properties_updater().
//##############################################################################

class properties_updater
{

  public:
    properties_updater( v_map_t& v_map )
    {

#ifdef NNP_DEFAULT_PROPERTIES_UPDATER_HPP
      f_vec.push_back( new detail::updater( v_map ) );
#endif

#ifdef NNP_INTEGRATED_CURRENTS_UPDATER_HPP
      f_vec.push_back( new detail::integrated_currents_updater( v_map ) );
#endif

#ifdef NNP_EXPOSURE_UPDATER_HPP
      f_vec.push_back( new detail::exposure::updater( v_map ) );
#endif

#ifdef NNP_METALLICITY_UPDATER_HPP
      f_vec.push_back( new detail::metallicity_updater( ) );
#endif

#ifdef NNP_NEUTRINOS_UPDATER_HPP
      f_vec.push_back( new detail::neutrinos_updater( v_map ) );
#endif

#ifdef NNP_CLASSICAL_CHEMICAL_POTENTIAL_UPDATER_HPP
      f_vec.push_back( new detail::classical_chemical_potential::updater( v_map ) );
#endif

#ifdef NNP_SPECIES_TIMESCALE_UPDATER_HPP
      f_vec.push_back( new detail::species_timescale_updater( v_map ) );
#endif

#ifdef NNP_NG_EQUIL_UPDATER_HPP
      f_vec.push_back( new detail::ng_equil::updater( v_map ) );
#endif

#ifdef NNP_MOMENT_ABUNDANCE_UPDATER_HPP
      f_vec.push_back( new moment_abundance_updater( v_map ) );
#endif

    }

    void operator()( nnt::Zone& zone )
    {
      BOOST_FOREACH( base::properties_updater& f, f_vec )
      {
        f( zone );
      }
    }

    void operator()( std::vector<nnt::Zone>& zones )
    {
      BOOST_FOREACH( base::properties_updater& f, f_vec )
      {
        f( zones );
      }
    }

    void initialize( nnt::Zone& zone )
    {
      BOOST_FOREACH( base::properties_updater& f, f_vec )
      {
        f.initialize( zone );
      }
    }

    void initialize( std::vector<nnt::Zone>& zones )
    {
      BOOST_FOREACH( base::properties_updater& f, f_vec )
      {
        f.initialize( zones );
      }
    }

  private:
    boost::ptr_vector<base::properties_updater> f_vec;

};

//##############################################################################
// options().
//##############################################################################

class options
{

  public:
    options(){}

    void
    get( options_map& o_map )
    {

      try
      {

        po::options_description options_desc( "\nProperties updater options");

        options_desc.add_options()

        ;

#ifdef NNP_DEFAULT_PROPERTIES_UPDATER_HPP
        o_vec.push_back( new detail::options() );
#endif

#ifdef NNP_INTEGRATED_CURRENTS_UPDATER_HPP
        o_vec.push_back( new detail::integrated_currents::options() );
#endif

#ifdef NNP_EXPOSURE_UPDATER_HPP
        o_vec.push_back( new detail::exposure_::options() );
#endif

#ifdef NNP_METALLICITY_UPDATER_HPP
        o_vec.push_back( new detail::metallicity::options() );
#endif

#ifdef NNP_CLASSICAL_CHEMICAL_POTENTIAL_UPDATER_HPP
        o_vec.push_back( new detail::classical_chemical_potential::options() );
#endif

#ifdef NNP_SPECIES_TIMESCALE_UPDATER_HPP
        o_vec.push_back( new detail::species_timescale::options() );
#endif

#ifdef NNP_NG_EQUIL_UPDATER_HPP
        o_vec.push_back( new detail::ng_equil::options() );
#endif

#ifdef NNP_MOMENT_ABUNDANCE_UPDATER_HPP
        o_vec.push_back( new detail::moment_abundance::options() );
#endif

        BOOST_FOREACH( base::options& f, o_vec )
        {
          f( options_desc );
        } 

        o_map.insert(
          std::make_pair<std::string, options_struct>(
            "properties_updater", 
            options_struct( options_desc )
          )
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

  private:
    boost::ptr_vector<base::options> o_vec;

};

}  // namespace properties_updater

}  // namespace wn_user

#endif // NNP_PROPERTIES_UPDATER_HPP
