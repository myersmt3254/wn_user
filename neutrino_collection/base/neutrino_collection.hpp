// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file neutrino_collection.hpp
//! \brief A file to define default neutrino routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include <boost/assign.hpp>
#include <boost/any.hpp>

#include "my_global_types.h"
#include "nnt/iter.h"

#ifndef NNP_NEUTRINO_COLLECTION_BASE_HPP
#define NNP_NEUTRINO_COLLECTION_BASE_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace base
{

//##############################################################################
// neutrino.
//##############################################################################

class neutrino
{
  public:
    neutrino(){}

    template<typename T>
    const T getProperty( std::string s_property )
    {
      std::map<std::string, boost::any>::iterator it;

      it = my_map.find( s_property );
      if( it == my_map.end() )
      {
        std::cerr << "Neutrino property " << s_property << " not found." <<
                     std::endl;
        exit( EXIT_FAILURE );
      }
      return boost::any_cast<T>( it->second );
    }

    void
    updateProperty( std::string s_property, boost::any prop )
    {
      my_map[s_property] = prop;
    }

  private:
    std::map<std::string, boost::any> my_map;

};

//##############################################################################
// neutrino_collection().
//##############################################################################

class neutrino_collection
{
  public:
    neutrino_collection()
    {

      v_neutrinos =
        boost::assign::list_of(ELECTRON_NEUTRINO)(ELECTRON_ANTINEUTRINO)
                              (MU_NEUTRINO)(MU_ANTINEUTRINO)
                              (TAU_NEUTRINO)(TAU_ANTINEUTRINO);

      BOOST_FOREACH( std::string s, v_neutrinos )
      {
        neutrino_map[s] = neutrino();
      }

    }

    neutrino& getNeutrino( std::string s_neutrino )
    {
      neutrino_map_t::iterator it;

      it = neutrino_map.find( s_neutrino );

      if( it == neutrino_map.end() )
      {
        std::cerr << s_neutrino << " is not a valid neutrino." << std::endl;
        exit( EXIT_FAILURE );
      }

      return it->second;
    }

    const std::vector<std::string> getNeutrinosVector()
    {
      return v_neutrinos;
    }

  private:
    typedef std::map<std::string, neutrino> neutrino_map_t;
    neutrino_map_t neutrino_map;
    std::vector<std::string> v_neutrinos;

};

}  // namespace base

}  // namespace wn_user

#endif  // NNP_NEUTRINO_COLLECTION_DETAIL_HPP
