////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define equilibrium routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include <boost/optional.hpp>
#include "equilibrium/base/equilibrium.hpp"

#ifndef NNP_EQUILIBRIUM_DETAIL_HPP
#define NNP_EQUILIBRIUM_DETAIL_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

#define S_CONSTRAINT       "constraint"
#define S_YE_EQ            "Ye_equil"

typedef std::map<std::string, double> cluster_map_t;

//##############################################################################
// equilibrium().
//##############################################################################

class equilibrium : public base::equilibrium
{

  public:
    equilibrium( v_map_t& v_map ) : base::equilibrium(), my_program_options(),
                                    my_cluster_computer()
    {
      if( v_map.count( S_CONSTRAINT ) )
      {
        BOOST_FOREACH(
          const std::vector<std::vector<std::string> >::value_type& v_y,
          my_program_options.composeOptionVectorOfVectors(
            v_map, S_CONSTRAINT, 2
          )
        )
        {
          cluster_map[v_y[0]] = boost::lexical_cast<double>( v_y[1] );
        }
      }
      if( v_map.count( S_YE_EQ ) )
      {
        dYe = v_map[S_YE_EQ].as<double>();
      }
    }

    void
    set( Libnucnet__Nuc * p_nuc )
    {
      base::equilibrium::set( p_nuc );
      if( dYe )
      {
        setYe( dYe.get() );
      }
      BOOST_FOREACH( cluster_map_t::value_type& t, cluster_map )
      {
        updateCluster( t.first, t.second );
      }
    }
      

  private:
    wn_user::program_options my_program_options;
    wn_user::cluster_abundance_moment_computer my_cluster_computer;
    cluster_map_t cluster_map;
    boost::optional<double> dYe = boost::none;

};
     
//##############################################################################
// equilibrium_options().
//##############################################################################

class equilibrium_options : public base::equilibrium_options
{

  public:
    equilibrium_options() : base::equilibrium_options() {}

    void
    get( po::options_description& equilibrium )
    {

      try
      {

        equilibrium.add_options()

        (
          S_CONSTRAINT,
          po::value<std::vector<std::string> >()->multitoken(),
          "Cluster constraint (enter as doublet (cluster xpath; abundance)"
        )

        (
          S_YE_EQ,
          po::value<double>(),
          "Equilibrium Ye"
        )

        ;

        base::equilibrium_options::get( equilibrium );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace detail

} // namespace wn_user 

#endif // NNP_EQUILIBRIUM_DETAIL_HPP
